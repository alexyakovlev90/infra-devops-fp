# CHANGELOG

- написать микросервисное приложение
- ~~поднять постгрес и кафка c zookeeper в докере для теста~~ - кафка в докере работала нестабильно
- завернуть сервисы приложения в докер
- развернуть приложение в minikube
- развернуть кластер k8s в gcloud
- ~~развернуть zookeeper и kafka в Kubernetes~~ - не увенчалось успехом 
- поднять и настроить постгрес и кафка для работы
- перевести проект в Gitlab
- настроить CI/CD
- доступ к ингрессам, балансировка
- поднять EFK для сбора логов
- сделать профили Stage, Prod
- поднять Kibana для просмотра логов
- прикрутить health check для сервисов со сваггером
- прикрутить к сервисам Spring Actuator для мониторинга
- Установить Prometheus
- установить Grafana, настроить дашборды

TODO:
- установка Postgres с помощью Ansible ролей
- установка Kafka + Zookeeper с помощью Ansible ролей
