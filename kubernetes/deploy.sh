#!/bin/bash

#kubectl apply -f postgres/postgres-configmap.yml
#kubectl apply -f postgres/postgres-deployment.yml
#kubectl apply -f postgres/postgres-service.yml


kubectl apply -f dev/dev-namespace.yml
kubectl create configmap env-config --from-env-file=../env-var.properties -n dev || true

kubectl apply -n dev -f dev/trx-producer/
kubectl apply -n dev -f dev/trx-processor/
kubectl apply -n dev -f dev/trx-receiver/


