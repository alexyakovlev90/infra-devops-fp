# Infrastructure DevOps

## Описание проекта

Есть условный платежный шлюз, через который идет поток транзакций.
Этот поток транзакций попадает в очередь, которую слушает сервис.
Слушатель очереди сохраняет все транзакции в БД, а на некоторые транзакции обрабатывает,
начисляет комиссию, баллы лояльности и т.д.

Проект состоит из 3х сервисов и визуально выглядит следующим образом:
![title](app_scheme.png)

В качестве мока платежного шлюза на схеме изображен сервис `trx-producer`,
который будет генерировать транзакции и складывать в очередь. Для увеличения 
нагрузки можно поднять несколько инстанцов сервиса. 

Прослушкой очереди занимается сервис `trx-receiver`. Его роль:
1) записать все пришедшие транзакции в БД
2) отправить часть транзакций на обработку в сервис `trx-processor`

Сервис `trx-processor` представляет собой простой REST сервис, 
содержащий контроллер с методами получения транзакций и бизнес логику обработки 
полученных транзакций. Обработанные транзакции сервис так же сохраняет в БД.


### trx-producer
- консольное приложение
- отправляет транзакции в Kafka по адресу `KAFKA_HOST` в топик `TOPIC_NAME`
- `KAFKA_HOST` по умолчанию _localhost:9092_
- `TOPIC_NAME` по умолчанию _sample-topic_

### trx-receiver
- стартует на 8080 порту
- слушает Kafka по адресу `KAFKA_HOST` из топика `TOPIC_NAME`
- пишет транзакции в Postgres `DB_HOST` в таблицу **TRX_DATA**
- отправляет транзакции в _trx-processor_
- `KAFKA_HOST` по умолчанию _localhost:9092_
- `TOPIC_NAME` по умолчанию _sample-topic_
- swagger доступен `trx-receiver:8080/swagger-ui.html`

### trx-processor
- стартует на 9090 порту
- получает транзакции по пути `/api/process`
- обрабатывает транзакции, записывает в `DB_HOST` в таблицу **TRX_DATA_PROCESSED**
- swagger доступен `trx-processor:9090/swagger-ui.html`


## Развертывание инфраструктуры
1) Развернем Kubernetes кластер
```bash
gcloud container clusters create otus-cluster \
    --machine-type n1-standard-1 \
    --num-nodes 2 \
    --disk-size 30 \
    --no-enable-stackdriver-kubernetes \
    --no-enable-autoupgrade \
    --enable-legacy-authorization \
    --project otus-fp
# нода помощнее для инфраструктуры сбора логов
gcloud container node-pools create bigpool \
      --cluster otus-cluster \
      --machine-type n1-standard-2 \
      --num-nodes 1 \
      --disk-size 40 \
      --no-enable-autoupgrade \
      --project otus-fp
gcloud container node-pools list --cluster otus-cluster
```
2) Установим серверную часть Helm’а - Tiller
```bash
kubectl apply -f tiller.yml
# Теперь запустим tiller-сервер
helm init --service-account tiller
# Check
kubectl get pods -n kube-system --selector app=helm
```
3) Из Helm-чарта установим ingress-контроллер nginx
```bash
helm install stable/nginx-ingress --name nginx
# Берем EXTERNAL-IP
kubectl get svc
# Добавляем EXTERNAL-IP nginx-nginx-ingress-controller в /etc/hosts
sudo bash -c 'echo "34.90.251.53 k8s-prometheus k8s-grafana k8s-kibana" >> /etc/hosts'
```
4) Инфраструктура для сбора логов
```bash
# Запуск EFK стека в k8s
kubectl apply -f ./kubernetes/efk/
# Установка Kibana
helm upgrade --install kibana stable/kibana \
    --set "ingress.enabled=true" \
    --set "ingress.hosts={k8s-kibana}" \
    --set "env.ELASTICSEARCH_URL=http://elasticsearch-logging:9200" \
    --version 0.1.1
```
- Создать шаблон индекса `fluentd-*`
- Поиск логов по лейблам:
```
kubernetes.labels.component:trx-receiver OR 
kubernetes.labels.component:trx-producer OR 
kubernetes.labels.component:trx-processor 
```
5) Мониторинг приложения
- prometheus загружен локально с помощью helm 
запускаетс с кастомными значениями custom_values.yml
```bash
# загрузка локально в kubernetes/
helm fetch --untar stable/prometheus
# запуск из kubernetes/prometheus/
helm upgrade prom . -f custom_values.yml --install
```
- в custom_values.yml также описаны джобы сбора метрик 
сервисов `trx-receiver` и `trx-processor` по пути `/actuator/prometheus`
- Поставим также grafana с помощью helm
```bash
helm upgrade --install grafana stable/grafana --set "adminPassword=admin" \
    --set "service.type=NodePort" \
    --set "ingress.enabled=true" \
    --set "ingress.hosts={k8s-grafana}"
```
- Дашборд для мониторинга k8s
https://grafana.com/grafana/dashboards/315
- На этом графике одновременно используются метрики и шаблоны из cAdvisor, 
и из kube-state-metrics для отображения сводной информации по деплойментам
https://grafana.com/dashboards/741



## Как запустить проект

1) Предполагается, что для проекта подняты Kafka и Postgres.
Сейчас Kafka и Postgres установлены с помощью Google Click to Deploy

3) Собираем артефакты (jar-файлы), билдим докер образы и пушим в докер хаб
Для локальной сборки потребуется
JDK8+ и Maven (https://www.baeldung.com/install-maven-on-windows-linux-mac)
```bash
bash docker-build-push.sh
```

4) Запуск приложения
```bash
# локально
bash docker-run.sh
# minikube
minikube start
bash kubernetes/deploy.sh
```

* Проверить работоспособность можно 
```
trx-processor:9090/swagger-ui.html
trx-receiver:8080/swagger-ui.html
```




